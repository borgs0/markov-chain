defmodule ParsingStream do

  @wordBoundaries String.graphemes(" ")
  @specialGraphemes String.graphemes("!?,.:;-\r\n\t") # These are interpreted as individual tokens.

  def from(text) do
    Stream.resource(
      fn -> text |> String.downcase |> String.graphemes end,
      fn text_list ->
        case next_token(text_list) do
          {"", _} -> {:halt, nil}
          {word, acc} -> {[word], acc}
        end
       end,
      fn _ -> nil end)
  end

  def next_token(text_list, partial_token \\ "")
  def next_token([], partial_token) do
    {partial_token, []}
  end
  def next_token(text_list, partial_token) do
    [char | rest] = text_list
    eq = &(&1 == char)
    cond do
      Enum.find(@specialGraphemes, eq) && partial_token == "" ->
        consume_word_boundary(rest, char)
      Enum.find(@specialGraphemes, eq) ->
        {partial_token, text_list}
      Enum.find(@wordBoundaries, eq) ->
        consume_word_boundary(rest, partial_token)
      true ->
        next_token(rest, partial_token <> char)
    end
  end

  def consume_word_boundary(text_list, partial_token) do
    unless text_list == [] do
      [char | rest] = text_list
      if Enum.find(@wordBoundaries, &(&1 == char)) do
        consume_word_boundary(rest, partial_token)
      else
        {partial_token, text_list}
      end
    end

    {partial_token, text_list}
  end
end
