defmodule Parsing do
  def count_combinations(text) do
    text |>
    find_combinations(2) |>
    count_duplicates
  end

  defp count_duplicates(words) do
    Enum.reduce(words, %{}, fn(word, dict) -> Map.update(dict, word, 1, &(&1 + 1)) end)
  end

  defp find_combinations(sentence, length) do
    ParsingStream.from(sentence) |>
    Enum.chunk(length, 1) |>
    Enum.map(&(List.to_tuple(&1)))
  end
end
