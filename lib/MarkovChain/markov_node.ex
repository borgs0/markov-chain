defmodule MarkovNode do
  defstruct count: 0, map: %{}

  def new(word, count) do
    map = Map.put_new(%{}, word, count)
    %MarkovNode { count: count, map: map}
  end

  def update(nil, second_word, count) do
    new(second_word, count)
  end

  def update(node, second_word, count) do
    map = Map.update(node.map, second_word, count, &(&1 + count))
    %MarkovNode{ count: node.count+count, map: map}
  end

  def merge(node1, node2) do
    dict = Map.merge(node1.map, node2.map, fn(_k, v1, v2) -> v1 + v2 end)
    %MarkovNode { count: node1.count + node2.count, map: dict}
  end

  # def roll_next(nil), do: nil
  def roll_next(node) do
    roll = :rand.uniform(node.count)
    Enum.reduce(node.map, 0, 
      fn(word_count, acc) ->
        {second_word, count} = word_count
        case acc do
          a when is_integer(a) and a + count >= roll -> second_word
          a when is_integer(a) -> a + count
          a -> a
        end
      end)
  end
end
