defmodule MarkovChainsManager do
  use GenServer

  defstruct servers: %{}, count: 0

  @keyDoesntExist {:error, "Key doesn't exist."}

  defp add_if_not_exists(manager, key) do
    case manager.servers[key] do
      nil ->
        {:ok, pid} = MarkovChainServer.start_link
        new_servers = Map.put(manager.servers, key, pid)
        %MarkovChainsManager{servers: new_servers, count: manager.count + 1}
      _   ->
        manager
    end
  end

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  ## Client API

  def get_keys(manager) do
    GenServer.call(manager, {:get_keys})
  end

  def get_count(manager) do
    GenServer.call(manager, {:get_count})
  end

  def get_current(manager, key) do
    GenServer.call(manager, {key, :get_current})
  end

  def dump(manager, key) do
    GenServer.call(manager,{key, :dump})
  end

  def set_current(manager, key, word) do
    GenServer.cast(manager, {key, :set_current, word})
  end

  def get_next(manager, key) do
    get_next(manager, {key, :get_next, 1})
  end
  def get_next(manager, key, length) do
    GenServer.call(manager, {key, :get_next, length})
  end

  def get_phrase(manager, key) do
    GenServer.call(manager, {key, :get_phrase})
  end

  def update(manager, key, pair_count) do
    GenServer.cast(manager, {key, :update, pair_count})
  end

  ## Getters
  def handle_call({:get_keys}, _from, manager) do
    {:reply, Map.keys(manager.servers), manager}
  end

  def handle_call({:get_count}, _from, manager) do
    {:reply, manager.count, manager}
  end

  def handle_call(params, _from, manager) when is_tuple(params) do
    [key | tail] = Tuple.to_list(params)
    call = List.to_tuple(tail)

    case manager.servers[key] do
      nil   -> {:reply, @keyDoesntExist, manager}
      chain -> {:reply, GenServer.call(chain, call), manager}
    end
  end

  def handle_cast(params, manager) when is_tuple(params) do
    [key | tail] = Tuple.to_list(params)
    call = List.to_tuple(tail)

    new_manager = add_if_not_exists(manager, key)
    chain_server = new_manager.servers[key]
    GenServer.cast(chain_server, call)
    {:noreply, new_manager}
  end

  ## Internal use only

  def init(:ok) do
    {:ok, %MarkovChainsManager{}}
  end
end
