defmodule MarkovChainServer do
  use GenServer

  @currentWordNotSet {:error, "Current word is not set."}

  ## Client API
  def start_link() do
    GenServer.start_link(__MODULE__, :ok)
  end

  ## Getters
  def handle_call({:dump}, _from, {word, chain}) do
    {:reply, {word, chain}, {word, chain}}
  end

  def handle_call({:get_current}, _from, {nil, chain}), do: {:reply, @currentWordNotSet, {nil, chain}}
  def handle_call({:get_current}, _from, {word, chain}) do
    {:reply, word, {word, chain}}
  end

  def handle_call({:get_next, _}, _from, {nil, chain}), do: {:reply, @currentWordNotSet, {nil, chain}}
  def handle_call({:get_next, length}, _from, {current, chain}) do
    words = MarkovChain.next(chain, current, length)
    {:ok, new_current} = Enum.fetch(words, -1)
    {:reply, words, {new_current, chain}}
  end

  def handle_call({:get_phrase}, _from, {current_word, chain}) do
    phrase = MarkovChain.get_phrase(chain)
    {:reply, phrase, {current_word, chain}}
  end

  def handle_call(:stop, _from, state), do: {:stop, :normal, :ok, state}

  # Setters
  def handle_cast({:set_current, word}, {_current, chain}) do
    {:noreply, {word, chain}}
  end

  def handle_cast({:update, pair_count}, {current, chain}) do
    {:noreply, {current, MarkovChain.update_range(chain, pair_count)}}
  end

  def init(:ok) do
    {:ok, {nil, MarkovChain.new}}
  end

  def handle_info({:DOWN, ref, :process, _from, _reason}, {names, refs}) do
    {name, refs} = Map.pop(refs, ref)
    names = Map.delete(names, name)
    {:noreply, {names, refs}}
  end

  def handle_info(_msg, state) do
    {:noreply, state}
  end
end
