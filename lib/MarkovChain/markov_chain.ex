defmodule MarkovChain do
  defstruct nodes: %{}

  @phraseBoundaries String.graphemes(".!?\n")

  def new() do
    %MarkovChain{nodes: %{}}
  end

  ## Accessors
  def get_node(chain, word) do
    chain.nodes[word]
  end

  ## Modifiers
  def update_range(network, pair_counts) do
    Enum.reduce(pair_counts, network, fn(pair, net) -> update(net, pair) end)
  end

  def update(chain, {{first_word, second_word}, count}) do
    do_update(chain, first_word, second_word, count)
  end

  defp do_update(chain, first_word, second_word, count) do
    new_nodes = Map.update(chain.nodes, first_word, MarkovNode.new(second_word, count),
      &(MarkovNode.update(&1, second_word, count)))
    %MarkovChain{ nodes: new_nodes }
  end

  def merge(network1, network2) do
    dict = Map.merge(network1.nodes, network2.nodes, fn(_k, v1, v2) -> MarkovNode.merge(v1, v2) end)
    %MarkovChain{nodes: dict}
  end

  ## Using the chain
  def next(network, word) do
    IO.puts word
    current_node = get_node(network, word)
    [MarkovNode.roll_next(current_node)]
  end

  def next(_, _, 0), do: []
  def next(network, word, length) do
    current_node = get_node(network, word)
    ## IO.puts word
    next_word = MarkovNode.roll_next(current_node)
    Enum.concat([next_word], next(network, next_word, length-1))
  end

  def get_phrase(network, current_word \\ "\n", acc \\ []) do
    current_node = get_node(network, current_word)
    next_word = MarkovNode.roll_next(current_node)

    cond do
      Enum.find(@phraseBoundaries, &(&1 == next_word)) ->
        Enum.concat(acc, [next_word]) |> List.to_string
      true ->
        get_phrase(network, next_word, Enum.concat(acc, [next_word]))
    end
  end

  ## Setting up the chain
  def calculate_network(text) when is_function(text) do
    text.() |>
    calculate_network
  end

  def calculate_network(text) do
    Parsing.count_combinations(text) |>
    reduce_to_network
  end

  defp reduce_to_network(results) do
    Enum.reduce(results, %MarkovChain{},
      fn(result, network) -> update(network, result) end)
  end
end
