defmodule MarkovChainTest do
  use ExUnit.Case

  setup do
    chain = MarkovChain.new
    {:ok, chain: chain}
  end

  test "MarkovChain.get empty", %{chain: chain} do
    assert MarkovChain.get_node(chain, "any_word") == nil
  end

  test "MarkovChain.merge empty", %{chain: chain} do
    assert chain == MarkovChain.merge(chain, chain)
  end
end
