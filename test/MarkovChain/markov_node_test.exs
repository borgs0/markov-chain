defmodule MarkovNodeTest do
  use ExUnit.Case

  test "MarkovNode.merge duplicates" do
    n1 = MarkovNode.new("word", 2)
    assert MarkovNode.new("word", 4) == MarkovNode.merge(n1, n1)
  end

  test "MarkovNode.merge distinct" do
    n1 = MarkovNode.new("word", 2)
    n2 = MarkovNode.new("other_word", 1)
    result = Map.put_new(Map.put_new(Map.new, "word", 2), "other_word", 1)
    assert %MarkovNode{count: 3, map: result} == MarkovNode.merge(n1, n2)
  end

  test "MarkovNode.merge with crossover" do
    n1 = MarkovNode.new("word", 2)
    n2 = %MarkovNode{count: 3, map: Map.put_new(Map.put_new(Map.new, "word", 2), "other_word", 1)}
    result = Map.put_new(Map.put_new(Map.new, "word", 4), "other_word", 1)
    assert %MarkovNode{count: 5, map: result} == MarkovNode.merge(n1, n2)
  end
end
