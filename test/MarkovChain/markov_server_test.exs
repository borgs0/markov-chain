defmodule MarkovChainServerTest do
  use ExUnit.Case

  setup do
    {:ok, server} = MarkovChainServer.start_link()
    {:ok, server: server}
  end

  test "Create a chain and perform some basic operations", %{server: server} do
    GenServer.cast(server, {:update, Parsing.count_combinations("thing here")})
    assert {:error, _} = GenServer.call(server, {:get_current})

    GenServer.cast(server, {:set_current, "thing"})
    assert "thing" = GenServer.call(server, {:get_current})
    assert ["here"] = GenServer.call(server, {:get_next, 1})
    assert "here" = GenServer.call(server, {:get_current})
  end

  test "Test chains longer than one", %{server: server} do
    parsed = Parsing.count_combinations("thing here is very large.")
    GenServer.cast(server, {:update, parsed})
    assert {:error, _} = GenServer.call(server, {:get_current})

    GenServer.cast(server, {:set_current, "thing"})
    assert ["here", "is", "very", "large"] = GenServer.call(server, {:get_next, 4})
    assert "large" = GenServer.call(server, {:get_current})
  end
end
