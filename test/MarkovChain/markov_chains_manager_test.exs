defmodule MarkovChainsManagerTest do
  use ExUnit.Case

  setup do
    {:ok, server} = MarkovChainsManager.start_link()
    {:ok, server: server}
  end

  test "Create a chain and perform some basic operations", %{server: server} do
    MarkovChainsManager.update(server, "key", Parsing.count_combinations("thing here"))
    assert {:error, _} = MarkovChainsManager.get_current(server, "key")

    MarkovChainsManager.set_current(server, "key", "thing")
    assert "thing"  = MarkovChainsManager.get_current(server, "key")
    assert ["here"] = MarkovChainsManager.get_next(server, "key", 1)
    assert "here"   = MarkovChainsManager.get_current(server, "key")
  end

  test "Test chains longer than one", %{server: server} do
    parsed = Parsing.count_combinations("thing here is very large.")
    MarkovChainsManager.update(server, "key", parsed)
    assert {:error, _} = MarkovChainsManager.get_current(server, "key")

    MarkovChainsManager.set_current(server, "key", "thing")
    assert ["here", "is", "very", "large"] = MarkovChainsManager.get_next(server, "key", 4)
    assert "large" = MarkovChainsManager.get_current(server, "key")
  end

  test "Can create multiple test chains", %{server: server} do
    key1 = "key"
    key2 = "not that key"

    parsed = Parsing.count_combinations("thing here is very large.")
    MarkovChainsManager.update(server, key1, parsed)
    assert 1 = MarkovChainsManager.get_count(server)

    MarkovChainsManager.update(server, key2, parsed)
    assert 2 = MarkovChainsManager.get_count(server)
  end

  test "Test chains are separate", %{server: server} do
    key1 = "key"
    key2 = "not that key"

    parsed1 = Parsing.count_combinations("thing here is very large.")
    parsed2 = Parsing.count_combinations("are you sure about that?")
    MarkovChainsManager.update(server, key1, parsed1)
    MarkovChainsManager.update(server, key2, parsed2)

    MarkovChainsManager.set_current(server, key1, "thing")
    MarkovChainsManager.set_current(server, key2, "are")

    assert ["here", "is", "very", "large"] = MarkovChainsManager.get_next(server, key1, 4)
    assert ["you", "sure", "about", "that"] = MarkovChainsManager.get_next(server, key2, 4)
    assert "large" = MarkovChainsManager.get_current(server, key1)
    assert "that" = MarkovChainsManager.get_current(server, key2)
  end
end
