defmodule ParsingTest do
  use ExUnit.Case

  setup do
    {:ok, text} = File.read("test/Parsing/parsing_test.txt")
    combos = Parsing.count_combinations(text)
    {:ok, combos: combos}
  end
end

# this is a ! thing!
# !
# this is a !? thing