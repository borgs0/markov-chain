stream = SlackMessageStream.from("general")
user_text = Enum.map(Enum.take(stream, 500), fn(%SlackMessage{user_id: user_id, text: text}) -> {user_id, text} end)

{:ok, manager} = MarkovChainsManager.start_link
pair_counts = Enum.map(user_text, fn({user, text}) -> {user, Parsing.count_combinations(text)} end)
Enum.each(pair_counts, fn({user, counts}) -> MarkovChainsManager.update(manager, user, counts) end)
MarkovChainsManager.get_phrase(manager, "U083CTFAM")
