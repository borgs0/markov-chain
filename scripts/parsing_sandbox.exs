String.downcase("This is a sentence, can't we all just get along?") |>
String.split |>
Enum.chunk(2, 1) |>
Enum.map(&(List.to_tuple(&1))) |>
IO.puts
